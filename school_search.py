import csv
import time
import itertools


INPUT_CSV_FILE = './school_data.csv'


class School(object):
    def __init__(self, name, city, state, score=None):
        self.name = name
        self.city = city
        self.state = state


def string_to_words(s):
    alpha_num_chars = [c for c in s.strip() if (c == ' ') or c.isalnum()]
    s = ''.join(alpha_num_chars)  # remove all non-alpha-numeric character, but spaces
    return [word.lower() for word in s.split(' ') if len(word.strip()) > 0]


class InvertedIndex(object):
    def __init__(self):
        self._word_to_ids = {}

    def add(self, s, id):
        for word in string_to_words(s):
            word = word.lower()
            if word not in self._word_to_ids:
                self._word_to_ids[word] = set([id])
            else:
                self._word_to_ids[word].add(id)

    def get_ids_contain_word(self, word):
        return self._word_to_ids.get(word.lower(), [])

    def search(self, query, top_k):
        id_cnts = {}
        for word in string_to_words(query):
            for id in self.get_ids_contain_word(word):
                if id not in id_cnts:
                    id_cnts[id] = 1
                else:
                    id_cnts[id] += 1
        id_scores = [(id, -cnt) for id, cnt in id_cnts.items()]  # score = -cnt <==> choose the ids that appear many times
        id_scores.sort(key=lambda x: x[1])  # sorted ASC by score
        top_k = min(top_k, len(id_scores))
        return [x[0] for x in id_scores[:top_k]]


def read_data():
    id_to_schools = {}
    inverted_index = InvertedIndex()
    with open(INPUT_CSV_FILE) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        is_first_row = True
        for row in reader:
            if is_first_row:  # header
                is_first_row = False
                continue
            id, agency_id, agency_name, name, city, state, lat, long, metro_code, urban_code, status = row
            id_to_schools[id] = School(name=name, city=city, state=state)
            inverted_index.add("%s %s %s" % (name, city, state), id)
    return id_to_schools, inverted_index


"""
    + This feature should search over school name, city name, and state name.
    + The top 3 matching results should be returned.
"""
def search_schools(query):
    start_time = time.time()
    school_ids = inverted_index.search(query=query, top_k=3)
    print('Results for "%s" (search took: %.3fs)' % (query, time.time()-start_time))
    i = 0
    for id in school_ids:
        i += 1
        school = id_to_schools[id]
        print("%d. %s\n%s, %s" % (i, school.name, school.city, school.state))
    print('\n')


id_to_schools, inverted_index = read_data()
