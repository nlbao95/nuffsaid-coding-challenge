import csv
import time
import itertools


INPUT_CSV_FILE = './school_data.csv'


"""
    + How many total schools are in this data set?
    + How many schools are in each state?
    + How many schools are in each Metro-centric locale?
    + What city has the most schools in it? How many schools does it have in it?
    + How many unique cities have at least one school in it?
"""
def print_counts():
    cnt_total = 0
    cnt_schools_in_metros = {}
    cnt_schools_in_states = {}
    cnt_schools_in_cities = {}
    with open(INPUT_CSV_FILE) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        is_first_row = True
        for row in reader:
            if is_first_row:  # header
                is_first_row = False
                continue
            cnt_total += 1
            id, agency_id, agency_name, name, city, state, lat, long, metro_code, urban_code, status = row
            if state not in cnt_schools_in_states:
                cnt_schools_in_states[state] = 1
            else:
                cnt_schools_in_states[state] += 1
            if metro_code not in cnt_schools_in_metros:
                cnt_schools_in_metros[metro_code] = 1
            else:
                cnt_schools_in_metros[metro_code] += 1
            if city not in cnt_schools_in_cities:
                cnt_schools_in_cities[city] = 1
            else:
                cnt_schools_in_cities[city] += 1
    print("Total Schools: %d" % cnt_total)
    print("\nSchools by State:")
    for state, cnt in cnt_schools_in_states.items():
        print("%s: %d" % (state, cnt))
    print("\nSchools by Metro-centric locale")
    for metro, cnt in cnt_schools_in_metros.items():
        print("%s: %d" % (metro, cnt))
    max_city = ''
    max_city_cnt = 0
    for city, cnt in cnt_schools_in_cities.items():
        if cnt > max_city_cnt:
            max_city_cnt = cnt
            max_city = city
    print("City with most schools: %s (%d schools)" % (max_city, max_city_cnt))
    print("Unique cities with at least one school: %d" % len(cnt_schools_in_cities))
